var Greeter = (function () {
    function Greeter(canvas) {
        this.img = new Image();
        this.pause = false;
        this.dots = [];
        this.ele = document.querySelectorAll(".ele");
        this.derection = true;
        Const.gthis = this;
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.eachEle();
    }
    Greeter.prototype.getimgData = function (context, canvas, dr) {
        var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
        context.clearRect(0, 0, canvas.width, canvas.height);
        var dots = [];
        var canbreak = false;
        for (var x = 0; x < imgData.width; x += dr) {
            for (var y = 0; y < imgData.height; y += dr) {
                var i = (y * imgData.width + x) * 4;
                if (imgData.data[i + 3] > 128) {
                    var dot = new Dot(x - dr, y - dr, 0, dr, {
                        a: imgData.data[i],
                        b: imgData.data[i + 1],
                        c: imgData.data[i + 2]
                    }, canvas);
                    dots.push(dot);
                }
            }
        }
        console.log(dots.length);
        return dots;
    };
    //RAF = (function () {
    //    return window.requestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
    //        return window.setTimeout(callback, 1000 / 60);
    //    }
    //})();
    //Array.prototype.forEach = function (callback) {
    //    for (var i = 0; i < this.length; i++) {
    //        callback.call(this[i]);
    //    }
    //}
    Greeter.prototype.eachEle = function () {
        var ele = Const.gthis.ele;
        var canvas = Const.gthis.canvas;
        var dr = Const.gthis.dr;
        var context = Const.gthis.context;
        var img = Const.gthis.img;
        var initAnimate = Const.gthis.initAnimate;
        var dots = Const.gthis.dots;
        var getimgData = Const.gthis.getimgData;
        dr = 3;
        if (ele[Const.index].getAttribute('data-dr') !== null) {
            dr = parseInt(ele[Const.index].getAttribute('data-dr'));
        }
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (ele[Const.index].innerHTML.indexOf("img") >= 0) {
            img.src = ele[Const.index].getElementsByTagName("img")[0].src;
            Const.gthis.imgload(img, function () {
                context.drawImage(img, canvas.width / 2 - img.width / 2, canvas.height / 2 - img.height / 2);
                Const.gthis.dots = getimgData(context, canvas, dr);
                initAnimate(Const.gthis);
            });
        }
        else {
            var text = ele[Const.index].innerHTML;
            for (var i = 0; i < text.length; i++) {
                context.save();
                var fontSize = Math.random() * 100 + 100;
                context.font = fontSize + "px bold";
                context.textAlign = "center";
                context.textBaseline = "middle";
                var code = text.charAt(i);
                context.fillStyle = "rgba(" + (Math.random() * 125 + 130) + "," + (Math.random() * 125 + 130) + "," + (Math.random() * 125 + 130) + " , 1)";
                context.fillText(code, canvas.width / 2 - ((text.length / 2 - i) * 150), canvas.height / 2);
                context.restore();
            }
            dots = Const.gthis.getimgData(context, canvas, dr);
            initAnimate(Const.gthis);
        }
        Const.index < (ele.length - 1) ? Const.index++ : Const.index = 0;
    };
    Greeter.prototype.imgload = function (img, callback) {
        if (img.complete) {
            callback.call(img);
        }
        else {
            img.onload = function () {
                callback.call(this);
            };
        }
    };
    Greeter.prototype.initAnimate = function (gthis) {
        var getRandom = gthis.getRandom;
        var canvas = gthis.canvas;
        gthis.dots.forEach(function (dot, i, dots) {
            dot.x = getRandom(0, canvas.width);
            dot.y = getRandom(0, canvas.height);
            dot.z = getRandom(-Const.focallength, Const.focallength);
            dot.tx = getRandom(0, canvas.width);
            dot.ty = getRandom(0, canvas.height);
            dot.tz = getRandom(-Const.focallength, Const.focallength);
        });
        gthis.dots.sort(function (a, b) {
            return b.z - a.z;
        });
        gthis.dots.forEach(function (dot, i, dots) {
            dot.paint();
        });
        gthis.lastTime = new Date();
        gthis.animate();
    };
    Greeter.prototype.getRandom = function (a, b) {
        return Math.random() * (b - a) + a;
    };
    Greeter.prototype.animate = function () {
        var dots = Const.gthis.dots;
        var context = Const.gthis.context;
        var canvas = Const.gthis.canvas;
        var lastTime = Const.gthis.lastTime;
        var pause = Const.gthis.pause;
        var derection = Const.gthis.derection;
        var animateRunning = Const.gthis;
        var thisTime = +new Date();
        context.save();
        context.globalCompositeOperation = 'destination-out';
        context.globalAlpha = 0.1;
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.restore();
        var sulv = 0.1;
        dots.forEach(function (dot, i, dots) {
            //var dot = this;
            if (derection) {
                if (Math.abs(dot.dx - dot.x) < 0.1 && Math.abs(dot.dy - dot.y) < 0.1 && Math.abs(dot.dz - dot.z) < 0.1) {
                    dot.x = dot.dx;
                    dot.y = dot.dy;
                    dot.z = dot.dz;
                    if (thisTime - lastTime > 300) {
                        Const.gthis.derection = false;
                    }
                }
                else {
                    dot.x = dot.x + (dot.dx - dot.x) * sulv;
                    dot.y = dot.y + (dot.dy - dot.y) * sulv;
                    dot.z = dot.z + (dot.dz - dot.z) * sulv;
                    lastTime = +new Date();
                }
            }
            else {
                if (Math.abs(dot.tx - dot.x) < 0.1 && Math.abs(dot.ty - dot.y) < 0.1 && Math.abs(dot.tz - dot.z) < 0.1) {
                    dot.x = dot.tx;
                    dot.y = dot.ty;
                    dot.z = dot.tz;
                    Const.gthis.pause = true;
                }
                else {
                    dot.x = dot.x + (dot.tx - dot.x) * sulv;
                    dot.y = dot.y + (dot.ty - dot.y) * sulv;
                    dot.z = dot.z + (dot.tz - dot.z) * sulv;
                    Const.gthis.pause = false;
                }
            }
        });
        dots.sort(function (a, b) {
            return b.z - a.z;
        });
        dots.forEach(function (dot, i, dots) {
            dot.paint();
        });
        if (!Const.gthis.pause) {
            //this.RAF(this.animate);
            window.setTimeout(function () {
                Const.gthis.animate();
            }, 1000 / 60);
        }
        else {
            context.clearRect(0, 0, canvas.width, canvas.height);
            Const.gthis.eachEle();
            Const.gthis.derection = true;
            Const.gthis.pause = false;
        }
    };
    return Greeter;
})();
var Dot = (function () {
    function Dot(centerX, centerY, centerZ, radius, color, canvas) {
        this.dx = centerX;
        this.dy = centerY;
        this.dz = centerZ;
        this.tx = 0;
        this.ty = 0;
        this.tz = 0;
        this.z = centerZ;
        this.x = centerX;
        this.y = centerY;
        this.radius = radius;
        this.color = color;
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
    }
    Dot.prototype.paint = function () {
        this.context.save();
        var scale = (this.z + Const.focallength) / (2 * Const.focallength);
        this.context.fillStyle = "rgba(" + this.color.a + "," + this.color.b + "," + this.color.c + "," + scale + ")";
        this.context.fillRect(this.canvas.width / 2 + (this.x - this.canvas.width / 2) * scale, this.canvas.height / 2 + (this.y - this.canvas.height / 2) * scale, this.radius * scale * 2, this.radius * scale * 2);
        this.context.restore();
    };
    return Dot;
})();
window.onload = function () {
    var canvas = document.getElementById("cas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var greeter = new Greeter(canvas);
};
//# sourceMappingURL=app.js.map